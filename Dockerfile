FROM richarvey/nginx-php-fpm:latest

# Configurations
ENV INSTALL_APP_LIST "vim fish"
ENV NGINX_WEBROOT "/var/www/html/grav"
ENV GRAV_VERSION "1.3.10"
ENV ADMIN_FULLNAME "Adminy McAdminface"
ENV ADMIN_USERNAME "admin"
ENV ADMIN_PASSWORD "ChangeMe"
ENV ADMIN_EMAIL "admin@whm.com"

# Link folders
WORKDIR /app
ADD ./nginx /app/nginx
ADD ./scripts /app/scripts

# Install required apps
# RUN apk add $INSTALL_APP_LIST

# Download Grav release, extract and move it to NGINX webroot
RUN wget https://github.com/getgrav/grav/releases/download/$GRAV_VERSION/grav-admin-v$GRAV_VERSION.zip -P /tmp
RUN unzip -q /tmp/grav-admin-v$GRAV_VERSION.zip -d /tmp
RUN mv /tmp/grav-admin $NGINX_WEBROOT

# Symbolically link the site-available config
RUN rm -rf /etc/nginx/sites-available
RUN ln -sf /app/nginx/sites-available /etc/nginx/

# Create admin user
RUN chmod +x /app/scripts/create_admin_user.sh
RUN /app/scripts/create_admin_user.sh

# Fix the grav  permissions
RUN chmod +x /app/scripts/fix_grav_permissions.sh
RUN /app/scripts/fix_grav_permissions.sh

# Make Workers History Museum the active theme
RUN sed -i "s/theme: antimatter/theme: workers-history-museum/g" /var/www/html/grav/user/config/system.yaml

# Clone the theme repo
RUN wget https://gitlab.com/whm-contributors/whm-grav-theme/repository/master/archive.zip -P /tmp
RUN unzip -q /tmp/archive.zip $NGINX_WEBROOT/user/themes
RUN rm /tmp/archive.zip

