#!/bin/sh
cd /var/www/html/grav
chown -R www-data:www-data /var/www/html/grav
find /var/www/html/grav -type f | xargs chmod 664
find /var/www/html/grav/bin -type f | xargs chmod 775
find /var/www/html/grav/ -type d | xargs chmod 775
find /var/www/html/grav/ -type d | xargs chmod +s
chmod a+w -R /var/www/html/grav/cache /var/www/html/grav/logs /var/www/html/grav/images /var/www/html/grav/user /var/www/html/grav/assets /var/www/html/grav/backup /var/www/html/grav/tmp
